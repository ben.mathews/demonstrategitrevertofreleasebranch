# demonstrateGitRevertOfReleaseBranch

This sequence of commands demonstrates

1. Initializing a new git repository.
2. Making an initial commit on master.
3. Creating a feature branch and making changes on it.
4. Merging the feature branch to release.
5. Making _additional_ changes on the release branch.
6. Reverting all the changes on the release branch in a single revert commit.
7. Merging the reversion back to master.

All history is preserved and git conventions are maintained. The graph of changes will look like this.

```
❯ git log --graph --decorate --oneline
*   2d61a19 (HEAD -> release, master) Merge branch 'release' into 'master'
|\
| * c6717de revert changes in release to master
| * 414e10d demo change on the release branch
| * d97d02e (feature/revertdemo) demo change
|/
* ac44446 first commit
```


```sh
mkdir tmp
cd tmp
git init
touch README.md
git add README.md
git commit -m "first commit"
git checkout -b release 
git checkout -b feature/revertdemo
touch foofile
git add foofile 
git commit -m "demo change"
git checkout release
git merge feature/revertdemo
touch barfile
git add barfile 
git commit -m "demo change on the release branch"

# Create a temporary branch off master
git checkout -B new-release master

# update the head head to release without affecting the working tree
git reset --soft release
git commit -m "revert changes in release to master"
git checkout release

# Merge in the reverted code
git merge --ff-only -m "merge reverted changes back to master" new-release 

# Clean up
git branch -d new-release

# Make sure that release and master are pointed at the same thing
git checkout master
git merge -s ours release -m "Merge branch 'release' into 'master'"
git checkout release
git merge master
```
